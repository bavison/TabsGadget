# TabsGadget
Toolbox Tabs gadget for RISC OS

Welcome to the open source release of the Tabs Toolbox gadget.

I've uploaded the source to github using a RISC OS style directory structure (eg ".c" files are in a directory called "c"), and LanManFS style file names (eg an Obey file will be called "file,feb"). My assumption is that anyone downloading from github will be doing so on Windows/Linux and then transferring the files to RISC OS using SMB of some sort. If you download a ZIP of the entire source tree, then extract it on the Windows/Linux side before copying files to RISC OS, then LanManFS will get the correct file types.

Build instructions:

Tabs was written using the Acorn (later ROOL) development environment, and the Norcroft compiler. It needs the standard C library and the Toolbox libraries - both the header files for compilation and the libraries to link against.

There used to be a dependency on an ancient product called "Dr Smith's C Development Toolkit" which was something like "valgrind" for RISC OS. But for this final release I've commented out any dependencies on that product, so you should be able to compile the module with just a standard "Acorn C/C++" development environment.

I've also removed a dependency on Acorn's "glib", simply by including the required files from the ROOL source in this distribution. There are the "glib", "glib3" and "rmensure" source files - they are all Apache licensed and copyright Acorn. These files were taken from RISCOS.Sources.Toolbox.Gadgets in the ROOL source tree.

I've never tried building this under any other environment, or with any other compiler, but I'm sure someone will make that work!

To build, just run the "!MkRam" taskobey file.
To clean, just run the "!MkClean" taskobey file.

Rik Griffin Jan 2022



Known issues in this version:

Events on a nested window return the wrong parent_object in the IDBlock,
but the correct parent_component.
